FROM python:3.9.7-slim-buster

# install dependency
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt --trusted-host=pypi.python.org --trusted-host=pypi.org --trusted-host=files.pythonhosted.org 

# Set TimeZone to Jakarta
ENV TZ=Asia/Jakarta
RUN apt-get install -y tzdata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

# deploy app
COPY wedding app
WORKDIR /app

EXPOSE 80
CMD ["python", "server.py"]